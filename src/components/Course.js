// dependencies
 import React, { useState } from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

export default function Course(props){
	/*
		Props - short term for properties. similar to the arguments/parameters found inside the functions. a way for the parent component to receive information.

			through the use of props, devs can use the same component and feed different information/data for rendering

	*/
	let course = props.course;

	/*
		useState() used in React to allow components to create manage its own data and is meant to be used internally.
			-accepts an argument that is meant to be the value of the first element in array.

		in React.js, state values must not be changed directly. All changes to the state values must be through the setState() function.
			-setState() is the second element in the create array

		the enrollees will start at zero, the result of the useState() is an array of data that is then destructured into count and setCount.

		setCount function is used to update the value of the count variable, depending on the times that the enroll function is triggered by the onClick command (button event)
	*/

	const [ count, setCount ] = useState(0);
	const [ seat, setSeat ] = useState(10);

	function enroll(){
		if (seat > 0){
		setCount(count +1)
		setSeat(seat -1)
		} else{
			alert('No more seats.')
		}
	}


	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={12}>
				<Card>
					<Card.Body>
						<Card.Title>{course.name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{course.description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {course.price}</Card.Text>
						<h6>Enrollees</h6>
						<p>{count} Enrollees</p>
						<Button variant="primary" onClick={enroll}>Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}